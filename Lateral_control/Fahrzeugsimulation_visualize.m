% -------------------------------------------------------------------------
% VISUALIZATION
% -------------------------------------------------------------------------
addpath('../plotfunctions'); % Pfad zu den Visualisierungsfunktionen

% --------------  Interface zwischen simout und Vizard ------------------ %
% Kombiniere zu einer Struktur
l_h         = 1.420;            % Abstand Schwerpunkt zu Hinterachse (m)
md.ego_x        = x + l_h * cos(psi); % The Center of Gravity will be plotted
md.ego_y        = y + l_h * sin(psi);
md.ego_psi      = psi;
md.ego_v        = v;
md.ego_delta    = delta;
md.ego_stwa     = stwa;
md.a_x          = 0.0*delta; % not important
md.a_y          = 0.0*delta; % not important
md.t            = time;
% ----------------------------------------------------------------------- %
Vizard(md,[],...
        ... % First figure
        @(md,timeIndex,h)plot_birdview(md,timeIndex,h), [.0 .0 1.0 0.8],  1,...
        @(r,t,h) plot_tacho_with_gear(r,t,h,'ego_v','km/h',3.6), [.75 .72 .25 .3],  1,...
        @plot_steeringwheel, [.25 .72 .25 .3], 1,...
        @plot_kamm_circle, [.5 .72 .25 .3], 1);%,...
        ... % Second figure
        %@plot_delta_beta_r_v, [.0 .0 1 1], 2);
