%%
function [ stwa ] = feedback_law_students( d,theta_r,kappa_r, psi ) %#eml
% Vehicle parameters
stw_ratio = 18; %   Steering ratio
l = 2.9680; % l_h + l_v

stwa = 3*d; % won't work...

end

function alpha = normalizeAngle(alpha)
% alpha in -pi..pi
    alpha = mod(alpha+pi, 2*pi) -pi;
end % function